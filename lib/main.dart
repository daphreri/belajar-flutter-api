import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List newsData;
  Future getData() async {
    http.Response response = await http.get(
        "http://snipbisa.com/api/index?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ2OCwiaXNzIjoiaHR0cDovL3NuaXBiaXNhLmNvbS9hcGkvYXV0aC9zaWduaW4iLCJpYXQiOjE1NjE4NjAwMTcsImV4cCI6MTU2MjM4NTYxNywibmJmIjoxNTYxODYwMDE3LCJqdGkiOiJoUVdVaXExclB4aEhKZXdiIn0.OZPLPTtQ9qfFUEtwlVIImgvdxBCPhJ7hDe3BdGcMuH0");
    setState(() {
      List newsDataMentah = json.decode(response.body);
      newsData = newsDataMentah.reversed.toList();
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Image.asset('assets/logo.png', fit: BoxFit.cover,),
        ),
        body: Container(
            height: 410,
            child: ListView.builder(
              itemCount: newsData == null ? 0 : newsData.length,
              itemBuilder: (context, int i) {
                return Image.network("http://snipbisa.com/laravel/storage/app/public/image/"+newsData[i]["image"], fit: BoxFit.cover,);
              },
            )));
  }
}